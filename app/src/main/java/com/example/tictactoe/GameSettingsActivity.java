package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class GameSettingsActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String EXTRA_LEVEL = "level";
    private EditText etNums;
    private Button btnStart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_settings);

        this.btnStart = (Button)findViewById(R.id.start_game_btn);
        this.etNums = (EditText)findViewById(R.id.et_set);

        this.btnStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String value = this.etNums.getText().toString();
        if(value.trim().equals("3") || value.trim().equals("4") || value.trim().equals("5"))
        {
            //Toast.makeText(this, "the board is " + value + "x" + value, Toast.LENGTH_LONG).show();
            int rows = Integer.parseInt(value.trim());
            Intent intent = new Intent(this, GameActivity.class);
            intent.putExtra(EXTRA_LEVEL, rows );
            startActivity(intent);
            finish();
        }
        else
        {
            Toast.makeText(this, "invalid value - enter a number between 3-5", Toast.LENGTH_SHORT).show();

        }
    }
}