package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnSave;
    private EditText e1player;
    private EditText e2player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btnSave = (Button)findViewById(R.id.btnMainSave);
        this.e1player = (EditText)findViewById(R.id.p1);
        this.e2player = (EditText)findViewById(R.id.p2);

    }
    public void btnSaveMain(View v)
    {
        String name1 = this.e1player.getText().toString();
        String name2 = this.e2player.getText().toString();

        //check that both names are filled
        if(name1.trim().equals("") || name2.trim().equals(""))
        {
            Toast.makeText(this, " Player ha to be filled", Toast.LENGTH_SHORT).show();
        }
        else
        {
            //Toast.makeText(this, "Player1 is " + name1 + " and player2 is " + name1, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, GameSettingsActivity.class);
            startActivity(intent);
            finish();
        }

    }
}